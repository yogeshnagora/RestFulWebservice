package com.minutes.rest.webservice.RestFulWebservice.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.minutes.rest.webservice.RestFulWebservice.model.HelloWorldBean;

@RestController
public class HelloController {

	@GetMapping(path="/hello-world")
	public String helloWorld(){
		return "Hello-World";
	}
	
	@GetMapping(path="/hello-world-bean")
	public HelloWorldBean helloWorldBean(){
		return new HelloWorldBean("hello-world-bean");
	}
}
