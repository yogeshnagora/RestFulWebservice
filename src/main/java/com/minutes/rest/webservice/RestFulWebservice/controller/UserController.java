package com.minutes.rest.webservice.RestFulWebservice.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.minutes.rest.webservice.RestFulWebservice.dao.UserDao;
import com.minutes.rest.webservice.RestFulWebservice.exception.DataNotFoundException;
import com.minutes.rest.webservice.RestFulWebservice.model.User;

@RestController
public class UserController {

	@Autowired
	UserDao userDao;
	
	@GetMapping(path = "/user")
	public List<User> findAll(){
		return userDao.findAll();
	}
	
	@GetMapping(path = "/user/{id}")
	public Resource<User> findOne(@PathVariable Integer id){
		User user= userDao.findOne(id);
		if(user == null){
			throw new DataNotFoundException("id-"+id);
		}
		
		//HATEOAS - Start
		//Link finall, serverpath+"/user"
		System.out.println(user);
		Resource<User> resource = new Resource<User>(user);
		
		ControllerLinkBuilder linkTO= linkTo(methodOn(this.getClass()).findAll());
		resource.add(linkTO.withRel("all-user"));
		System.out.println("resources "+resource);
		return resource;
		//return user;
	}
	
	@DeleteMapping(path = "/user/{id}")
	public ResponseEntity<Object> deleteById(@PathVariable Integer id){
		User user= userDao.DeleteById(id);
		if(user == null){
			throw new DataNotFoundException("user not found for id - "+id);
		}
		/*URI location = ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(user.getId())
				.toUri();*/
		return ResponseEntity.noContent().build();
		
	}
	
	
	@PostMapping(path="/user")
	public ResponseEntity<User> Save(@Valid @RequestBody User user){
		
		User saveduser= userDao.save(user);
		
		URI location= ServletUriComponentsBuilder
		.fromCurrentRequest()
		.path("/{id}")
		.buildAndExpand(saveduser.getId())
		.toUri();
		
		return ResponseEntity.created(location).build();
		//return ResponseEntity.ok().build();
	}
	
}
