package com.minutes.rest.webservice.RestFulWebservice.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.minutes.rest.webservice.RestFulWebservice.model.SampleBean;

@RestController
public class FilterController {

	//field1, filed2
	@GetMapping(path ="/filter")
	public MappingJacksonValue getSampleBean(){
		SampleBean sample= new SampleBean("Value1", "Value2", "Value3");
		SimpleBeanPropertyFilter filter =SimpleBeanPropertyFilter.filterOutAllExcept("field1","field2");
		FilterProvider filters= new SimpleFilterProvider().addFilter("SampleBeanFilter", filter);
		MappingJacksonValue mapping = new MappingJacksonValue(sample);
		mapping.setFilters(filters);
		return mapping;
	}
	
	//field2, field3
	@GetMapping(path ="/filter-list")
	public MappingJacksonValue getSampleBeanList(){
		List<SampleBean> list= Arrays.asList(new SampleBean("Value1", "Value2", "Value3"),new SampleBean("Value11", "Value22", "Value33"));
		
		SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter.filterOutAllExcept("field2","field3");
		FilterProvider filters = new SimpleFilterProvider().addFilter("SampleBeanFilter", filter);
		MappingJacksonValue mapping = new MappingJacksonValue(list);
		mapping.setFilters(filters);
		return mapping;
	}
}
