package com.minutes.rest.webservice.RestFulWebservice.exception;

import java.util.Date;

public class ExceptionResponse {

	private Date timestamp;
	private String message;
	private String deatil;
	
	public ExceptionResponse(Date timestamp, String message, String deatil) {
		super();
		this.timestamp = timestamp;
		this.message = message;
		this.deatil = deatil;
	}
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getDeatil() {
		return deatil;
	}
	public void setDeatil(String deatil) {
		this.deatil = deatil;
	}
	@Override
	public String toString() {
		return "ExceptionResponse [timestamp=" + timestamp + ", message=" + message + ", deatil=" + deatil + "]";
	}
	
	
}
