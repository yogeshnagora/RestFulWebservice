package com.minutes.rest.webservice.RestFulWebservice.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.springframework.stereotype.Component;

import com.minutes.rest.webservice.RestFulWebservice.model.User;

@Component
public class UserDao {

	private Integer userCount=3;
	private static List<User> userlist= new ArrayList<>();
	
	static{
		userlist.add(new User(1, "yogesh", new Date()));
		userlist.add(new User(2, "rahul", new Date()));
		userlist.add(new User(3, "kuku", new Date()));
	}
	
	public List<User> findAll(){
		return userlist;
	}
	
	public User findOne(int id){
		for (User user : userlist) {
			if(id == user.getId()){
				return user;
			}
		}
		return null;
	}
	
	public User DeleteById(int id){
		Iterator<User> iterator = userlist.iterator();
		
		while (iterator.hasNext()) {
			User user = (User) iterator.next();
			if(id == user.getId()){
				iterator.remove();
				return user;
			}
		}
		
		for (User user : userlist) {
			if(id == user.getId()){
				return user;
			}
		}
		return null;
	}
	
	public User save(User user){
		if(user.getId()==null){
			user.setId(++userCount);
		}
		userlist.add(user);
		return user;
	}
}
